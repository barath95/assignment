package Runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import cucumber.api.testng.AbstractTestNGCucumberTests;


@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/java/features/Test.feature", 
				//format = { "pretty", "json:target/Result.json","html:target/site/cucumber-pretty" ,"html:test-output"},
				glue = {"stepsDefinitions"},
				tags = "@Test"
			)
public class TestRunner {



}
















