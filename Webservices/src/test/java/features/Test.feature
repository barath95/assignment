Feature: Trimble Assignment

  Background: Validating the List of Users
    Given Pass an ENDPOINT_baseURI "BaseUrl"

  @Test
  Scenario: Get and Delete request
    Given Declare a new Request
    When Send get request to service "GetWebServiceName"
    Then The response status code should be "200"
    And Writing in a file
    And Fetch below parameters from Json Response Body
      | page        | page        |
      | per_page    | per_page    |
      | total       | total       |
      | total_pages | total_pages |
      | data        | data        |
      
    Then Send Delete request to service "DeleteUser"
    Then The response status code should be "204"

  @Test
  Scenario Outline: Verify Put request
    Given Declare a new Request
    And Pass below Header parameters
      | Content-Type | application/json |
    And Pass Json String as Request Body "JSONString" with "<FirstName>" and "<LastName>"
    When Send Put request to service "UpdateUser"
    Then Verify the response status code should be 200 and response body contains "<FirstName>" and "<LastName>"
    And Writing in a file

    Examples: 
      | FirstName | LastName |
      | bharathi  | selvan   |
