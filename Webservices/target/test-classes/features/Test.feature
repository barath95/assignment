Feature: Validating QSS Customer services

 
  Background: Verify Get Request
    Given We have an ENDPOINT_baseURI "BaseUrl"
    
    @Test
    Scenario: Verify
    Given We declare a new Request
    #And We have below Path parameters
  #    | fname | bharathi |
  #    | lname | selvan   |
    When we send Get request to service "GetWebServiceName"
    Then The response status code should be 200
    And Writing in a file
    And We fetch below parameters from Json Response Body
      | page        | page        |
      | per_page    | per_page    |
      | total       | total       |
      | total_pages | total_pages |
      | data        | data        |
      
    
    Then We send Delete request to service "DeleteUser"
    Then The response status code should be "204"
    
    #Given We declare a new Request
   # And We have below Path parameters
    #  | first_name | bharathi |
    #  | last_name | selvan   |
    Then We send Put request to service "UpdateUser"
    Then The response status code should be 200
   # And Writing in a file
